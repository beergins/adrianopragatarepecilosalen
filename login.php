<?php
 // Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    header("location: index.php");
    exit;
}

// Include config file
require_once "config.php";

// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Check if username is empty
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter username.";
    } else {
        $username = trim($_POST["username"]);
    }

    // Check if password is empty
    if (empty(trim($_POST["password"]))) {
        $password_err = "Please enter your password.";
    } else {
        $password = trim($_POST["password"]);
    }

    // Validate credentials
    if (empty($username_err) && empty($password_err)) {
        // Prepare a select statement
        $sql = "SELECT id, username, password FROM users WHERE username = ?";

        if ($stmt = mysqli_prepare($link, $sql)) {
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);

            // Set parameters
            $param_username = $username;

            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                // Store result
                mysqli_stmt_store_result($stmt);

                // Check if username exists, if yes then verify password
                if (mysqli_stmt_num_rows($stmt) == 1) {
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                    if (mysqli_stmt_fetch($stmt)) {
                        if (password_verify($password, $hashed_password)) {
                            // Password is correct, so start a new session
                            session_start();

                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;

                            // Redirect user to welcome page
                            header("location: index.php");
                        } else {
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else {
                    // Display an error message if username doesn't exist
                    $username_err = "No account found with that username.";
                }
            } else {
                echo "Oops! Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Close connection
    mysqli_close($link);
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login Page</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nanum+Pen+Script" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=ZCOOL+KuaiLe" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Patrick+Hand" rel="stylesheet">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/web-layout.css">
    <style type="text/css">
        .h1 {
            font-size: 60px;
            font-family: 'Gloria Hallelujah', cursive;
            text-shadow: 2px 4px #525151;
        }

        .logo {
            height: 5em;
            width: 5em;
            float: left;
            margin-left: 10px;
        }

        .logoname {
            height: 5em;
            width: 41em;
            float: center;
            margin-left: -15px;
        }

        .orb {
            display: inline-block;
            transition: all 2s ease-out;
        }

        .orb:hover {
            transform: scale(1.5);
            opacity: 1;
            cursor: pointer;
            animation-play-state: paused;
        }

        .margins {
            margin: -30px;
        }

        .bg-color {
            background: #b5bdc8;
            background: -moz-linear-gradient(top, #b5bdc8 0%, #828c95 19%, #28343b 100%);
            background: -webkit-linear-gradient(top, #b5bdc8 0%, #828c95 19%, #28343b 100%);
            background: linear-gradient(to bottom, #b5bdc8 0%, #828c95 19%, #28343b 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#b5bdc8', endColorstr='#28343b', GradientType=0);
        }

        @media only screen and (max-width: 600px) {
            .logo {
                height: 5em;
                width: 5em;
                float: left;
                margin-left: 5px;
            }

            .logoname {
                height: 5em;
                width: 16em;
                float: center;
                margin-left: 5px;

            }
        }
    </style>

    <script src="js/vendor/modernizr-2.6.2.min.js"></script>

</head>

<body class="bg-color">


    <div class="site-bg"></div>
    <div class="site-bg-overlay"></div>
    <!-- TOP HEADER -->
    <div class="top-header" style="height: 7em">
        <img src="images/logo.png" alt="" class="logo">
        <div class="container">
            <img src="images/patalino.png" alt="" class="logoname">
        </div>

    </div> <!-- .top-header -->
    <div class="container" id="page-content">

        <div class="row margins">
            <div class="col-md-9 col-sm-12 content-holder">
                <!-- CONTENT -->
                <div id="menu-container">
                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <div class="box-content">
                                <h3 class="widget-title">Log In</h3>

                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                                    <fieldset>
                                        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                                            <label>Username</label>
                                            <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                                            <span class="help-block"><?php echo $username_err; ?></span>
                                        </div>
                                        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                                            <label>Password</label>
                                            <input type="password" name="password" class="form-control">
                                            <span class="help-block"><?php echo $password_err; ?></span>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" value="Login">
                                        </div>
                                        <p>Don't have an account? <a href="signup.php">Sign up now</a>.</p>
                                </form>


                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="box-content">
                                <h3 class="widget-title">Log In</h3>
                                <p>Log in to experience a fast and quick thinking basic math solving game. After loggin in, click the game icon to play, Enjoy!</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>


        </div>
    </div>

    <!-- SITE-FOOTER -->
    <div class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>
                        <strong>Copyright &copy; 2019 <a href="#">PARS</a>.</strong> All rights reserved.

                    </p>
                </div>
            </div>
        </div>
    </div> <!-- .site-footer -->

    <script src="js/vendor/jquery-1.10.2.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
</body>

</html> 