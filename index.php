<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">	
		<link href="https://fonts.googleapis.com/css?family=Nanum+Pen+Script" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=ZCOOL+KuaiLe" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Patrick+Hand" rel="stylesheet">
        
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/web-layout.css">
		<style type="text/css">
			.h1{
				font-size: 60px;
				font-family: 'Gloria Hallelujah', cursive;
				text-shadow: 2px 4px #525151;
			}			
			.logo{
				height: 5em;
				width: 5em;
				float: left;
				margin-left: 10px;
			}
			.logoname{
				height: 5em;
				width: 30em;
				float: center;
				margin-left: -200px;
			}
			.orb {			
				display: inline-block;				
				transition: all 2s ease-out;
			}
			.orb:hover {
				transform: scale(1.5);
				opacity: 1;
				cursor: pointer;
				animation-play-state: paused;
            }
			.margins{
				margin: -30px;
			}
            .bg-color{
                background: #b5bdc8;
                background: -moz-linear-gradient(top, #b5bdc8 0%, #828c95 19%, #28343b 100%);
                background: -webkit-linear-gradient(top, #b5bdc8 0%,#828c95 19%,#28343b 100%);
                background: linear-gradient(to bottom, #b5bdc8 0%,#828c95 19%,#28343b 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b5bdc8', endColorstr='#28343b',GradientType=0 );
            }
			#logout{
					float: right;
					margin-top: 15px; 
			}
			@media only screen and (max-width: 600px){
				.logo{
				height: 5em;
				width: 5em;
				float: left;
				margin-left: 10px;
				}
				.logoname{
					height: 5em;
					width: 16em;
					float: center;
					margin-left: 15px;
				}
				#logout{
					float: right;
					margin: -100px 10px; 
				}
				.phone-info{
					font-size: 25px;
				}
			}
			
		</style>
		
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
		
    </head>
    <body class="bg-color">     
        <div class="site-bg"></div>
        <div class="site-bg-overlay"></div>
		<!-- TOP HEADER -->
        <div class="top-header" style="height: 7em">
			<img src="images/logo.png" alt="" class="logo"  >
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <br><p class="phone-info" >Welcome <a><?php echo htmlspecialchars($_SESSION["username"]); ?></a>!</p>
						
                    </div>      
						<img src="images/patalino.png" alt="" class="logoname">
						<a class="btn btn-primary" id="logout" href="logout.php">Logout</a>
 
                </div>                
            </div>            
        </div> <!-- .top-header -->


        <div class="visible-xs visible-sm responsive-menu">
            <a href="#" class="toggle-menu">
                <i class="fa fa-bars"></i> Show Menu
            </a>
            <div class="show-menu">
                <ul class="main-menu">
						<li>
							<a class="show-5 homebutton" href="#" ><i class="fa fa-home"></i>Home</a>						
                        </li>
                        <li>							
                            <a class="show-3 projectbutton" href="#"><i class="fa fa-bar-chart"></i>Ranks</a>
                        </li>
                </ul>
            </div>
        </div>

        <div class="container" id="page-content">
		            
            <div class="row margins" >
                <div class="col-md-12 col-sm-12 content-holder">
                    <!-- CONTENT -->
                    <div id="menu-container">
                                                
                        
                        <div id="menu-1" class="homepage home-section text-center">
                            <div class="welcome-text">
                                          
								<h2><strong><marquee id="myMarquee" scrollamount="10" loop="-1" >"Prove them wrong that you are right?"</marquee></strong></h2>
								<p>This is a Quick Maths Quiz website.</p><br/>
								<p><strong>INTRUCTIONS:</strong>
								<ul>
									<li>Answer as many as you can before the time runs out.</li>
									<li>Every question has 5 seconds for you to answer it correctly.</li>
									<li>Every incorrect answer will deduct 5 points from you total points.</li>
									<li>Once your time is up, it will deduct 10 points from your total points.</li>
									
								</ul>	
                                <h1  class="h1 orb" ><strong><a href="gameplay.php">Play</a></strong></h1>
                                
                            </div>
                        </div>				
                    </div>				
                </div>
            </div>
        </div>

             
            <!-- SITE-FOOTER -->
        <div class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>
                        	<strong>Copyright &copy; 2019 <a href="#">PARS</a>.</strong>  All rights reserved.
                        
						</p>
                    </div>
                </div>
            </div>
        </div> <!-- .site-footer -->

        <script src="js/vendor/jquery-1.10.2.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
