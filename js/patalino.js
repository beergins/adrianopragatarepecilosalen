class Question {
    //begining of class
    //constructor for this object
    constructor(num1, num2, oper, answer) {
        this.num1 = generateNum1();
        this.num2 = generateNum2();
        this.oper = generateOperator();
        //console.log("Initial values are: " + " " + this.num1 + " " + this.oper + " " + this.num2 );
        //conditional operators for division
        //if divisor is 0, reroll
        if (this.oper == '/' && this.num2 == 0) {
            //console.log("The divisor is 0,  rerolling.");
            this.num2 = getRandomIntInclusive(1, 10);
        }
        //if num1 > num2, reroll
        while (this.num2 > this.num1 && this.oper == '/') {
            //console.log("The divisor is larger than the dividend, rerolling.");
            this.num2 = generateNum2();
            if (this.num2 == 0) {
                this.num2 = getRandomIntInclusive(1, 10);
            }
        }
        //reroll if both 0 and oper is /
        while (this.num1 == 0 && this.num2 == 0 && this.oper == '/') {
            //console.log("Both the divisor and dividend are 0, rerolling.")
            this.num1 = generateNum1();
            this.num2 = generateNum2();
            this.oper = generateOperator();
        }
        //if n1%n2 !=0 , trap for getting perfect division.
        while (this.num1 % this.num2 != 0 && this.oper == '/') {
            //console.log("The numbers were not perfect division. Rerolling.");
            this.num1 = generateNum1();
            this.num2 = generateNum2();
        }
        switch (this.oper) {
            case '+':
                answer = this.num1 + this.num2;
                break;
            case '-':
                answer = this.num1 - this.num2;
                break;
            case '/':
                //answer = Math.round( ( (this.num1 / this.num2) * 100) / 100 ); this is for round off 
                answer = this.num1 / this.num2;
                break;
            case '*':
                answer = this.num1 * this.num2;
                break;
        }
        this.answer = answer;
        //console.log("Succesfully created a question object." + " " + this.num1 + " " + this.oper + " " + this.num2 + " = " + this.answer );
        //end of switch statement    
    }
    //end of class   
}
//globalized variables
var score = 0;
var correctAnswers = 0;
var wrongAnswers = 0;
var numberOfQuestions = 0;
var accuracy = 0;

//general functions
function generateNum1() {
    var num1 = Math.floor(Math.random() * 20) - 10;
    return num1;
}
function generateNum2() {
    var num2 = Math.floor(Math.random() * 10) - 10;
    return num2;
}
function generateOperator() {
    var operators = ['+', '-', '/', '*'];
    var opoutput = operators[Math.floor(Math.random() * operators.length)];
    return opoutput;
}
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//LOGIC FUNCTIONS
//TO GENERATE A QUESTION
function generateQuestion() {
    currentQuestion = new Question();
    //console.log(currentQuestion.num1 + " " + currentQuestion.oper + " " + currentQuestion.num2);
    switch (currentQuestion.oper) {
        case '+':
            question.innerHTML = "What is the sum of " + currentQuestion.num1 + " " + currentQuestion.oper + " " + currentQuestion.num2 + " ?";
            break;
        case '-':
            question.innerHTML = "What is the difference of " + currentQuestion.num1 + " " + currentQuestion.oper + " " + currentQuestion.num2 + " ?";
            break;
        case '/':
            question.innerHTML = "What is the quotient of " + currentQuestion.num1 + " " + currentQuestion.oper + " " + currentQuestion.num2 + " ?";
            break;
        case '*':
            question.innerHTML = "What is the product of " + currentQuestion.num1 + " " + currentQuestion.oper + " " + currentQuestion.num2 + " ?";
            break;

    }

    return currentQuestion;
    //end of generateQuestion()
}

//check answer function

function checkAnswer() {
    //variables
    var playeranswer = document.getElementById("playeranswer");
    //consolestatements   
    //console.log("Player answer is: " + playeranswer.value);
    //console.log("Question answer is: " + currentQuestion.answer);
    //logic
    if (playeranswer.value == currentQuestion.answer) {
        document.getElementById("imgstatus").src = "HappyNerd.png";
        statetext.innerHTML = "<span style=\"color:#37ff00\"> YOU ARE CORRECT! </span>";
        correctAnswers++;
        score += 10 * (timeleft + 1);
        //console.log("10 x " + timeleft + " = " + timeleft*10);
        scoreText.innerHTML = "Score : " + score;
        playeranswer.value = '';
        delete currentQuestion;
        generateQuestion();
        resetInterval();

    } else {
        playeranswer.value = '';
        document.getElementById("imgstatus").src = "AngryNerd.png";
        wrongAnswers++;
        score -= 10;
        document.getElementById("scoreText").innerHTML = "Score : " + score;
        statetext.innerHTML = "<span style=\"color:red\"> YOU ARE WRONG! </span>";
    }
    //end of checkAnswer();
}

//TIMER RELATED SHIT
var timeleft = 5;
var timerMechanism = setInterval(timerFunct, 1000);
function timerFunct() {
    document.getElementById("timer").innerHTML = "Question Time: " + timeleft + " seconds remaining";
    //console.log("The remaining time is : " + timeleft);
    timeleft -= 1;
    if (timeleft <= 0) {
        //clearInterval(timerMechanism);
        //console.log("The timer has stopped.");
        wrongAnswers++;
        document.getElementById("timer").innerHTML = "Time's Up! Next Question!"
        score -= 50;
        document.getElementById("scoreText").innerHTML = "Score : " + score;
        delete currentQuestion;
        generateQuestion();
        timeleft = 5;
    }
}

//GLOBAL TIMER!
var globaltimeleft = 10;
var globalClock = setInterval(globalTimerClock, 1000);
function globalTimerClock() {
    document.getElementById("globaltimer").innerHTML = "Game Time: " + globaltimeleft + " seconds remaining";
    globaltimeleft -= 1;
    if (globaltimeleft <= 0) {
        clearInterval(globalClock);
        clearInterval(timerMechanism);
        numberOfQuestions = correctAnswers + wrongAnswers;
        accuracy = Math.floor((correctAnswers / numberOfQuestions) * 100);
        document.getElementById("reset").disabled = false;
        document.getElementById("submit").disabled = true;
        document.getElementById("playeranswer").disabled = true;
        //console.log("The global timer has stopped.");
        document.getElementById("globaltimer").innerHTML = "GAME OVER!"
        document.getElementById("statetext").innerHTML = "Your score : " + score + "<br> Total Questions: " + numberOfQuestions
            + "<br> You got " + correctAnswers + " correct answers."
            + "<br> You got " + wrongAnswers + " wrong answers."
            + "<br> Your accuracy is : " + accuracy + "%."


            document.getElementById('scorehidden').value = score;
            document.getElementById("corrAnsHidden").value = correctAnswers;
            document.getElementById("wrongAnsHidden").value = wrongAnswers;
            document.getElementById("numQuesHidden").value = numberOfQuestions;
            document.getElementById("accuHidden").value = accuracy;

            console.log("scoreHidden" + scorehidden.value);

            document.forms["details"].submit()

    }
}

//timerreset
function resetInterval() {
    //console.log("The timer has been reset.");
    //clearInterval(timerMechanism);
    timeleft = 5;
}

//RESET GAME FUNCTION
function resetGame() {
    score = 0;
    numberOfQuestions = 0;
    wrongAnswers = 0;
    correctAnswers = 0;
    accuracy = 0;
    timeleft = 5;
    timerMechanism = setInterval(timerFunct, 1000);
    globaltimeleft = 60;
    globalClock = setInterval(globalTimerClock, 1000);
    document.getElementById("submit").disabled = false;
    document.getElementById("playeranswer").disabled = false;

    playeranswer.value = '';
    document.getElementById("statetext").innerHTML = ' ';
    document.getElementById("scoreText").innerHTML = "Score : 0";

    generateQuestion();

}

//store function to send in php
function saveUserTimes() {
    $.post("savesettings.php",
        {
            name: $("#userName").val(),
            amount: aGlobalVariable,
            times: '1,2,3,4,5,6,7',
        },
        function (data, status) {
            document.getElementById("saveWarningText").innerHTML = data;
            $("#saveWarningText").fadeIn(100);
            setTimeout(function () { $("#saveWarningText").fadeOut(100); }, 3000);
        });
}
    //end of script