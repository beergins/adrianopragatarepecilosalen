﻿-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: Feb 28, 2019 at 02:37 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `score` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `score`) VALUES
(1, 'paolo', '$2y$10$RL/1UbdFzIIYJ6GCx4RI8.hwuONEbR3Ng1kUKJys4rw7MQ4HRhCie', '2019-02-15 20:08:47', 200),
(2, 'gian', '$2y$10$uEqF9Yb/u4FBKDMhUxUE3ORwdmRc6qrJu/S2aZ1yP2Qgd8/wZRwlS', '2019-02-15 20:57:37', 230),
(3, 'MarcPogi', '$2y$10$Ch47ycoxOJuanOWWi1mi/uzcPQw1AaR1Rcal9xvZczrJeJvJ7Z.96', '2019-02-18 09:06:45', 475),
(4, 'Patalino', '$2y$10$egdpBS0LbZ2yPRKdH48a8exbomi1tQ1896haqKyDuWLveI8eJIXoa', '2019-02-18 16:55:03', 0);
