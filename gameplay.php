<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Game Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">	
		<link href="https://fonts.googleapis.com/css?family=Nanum+Pen+Script" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=ZCOOL+KuaiLe" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Patrick+Hand" rel="stylesheet">
       
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/web-layout.css">
		<style type="text/css">
			h1{
				font-size: 20px;
				color: white;
			}
			.h1{
				font-size: 60px;
				font-family: 'Gloria Hallelujah', cursive;
				text-shadow: 2px 4px #525151;
			}
			.logo{
				height: 5em;
				width: 5em;
				float: left;
				margin-left: 10px;
			}
			.logoname{
				height: 5em;
				width: 30em;
				float: center;
				margin-left: -200px;
			}
			.orb {			
				display: inline-block;				
				transition: all 2s ease-out;
			}
			.orb:hover {
				transform: scale(1.5);
				opacity: 1;
				cursor: pointer;
				animation-play-state: paused;
			}
			#logout{
					float: right;
					margin-top: 15px;
			}
			#statetext{
				font-size: 30px;
				margin-top: 50px;
               
			}
			#question{				
				font-family: 'Nanum Pen Script', cursive;
				font-size: 55px;
			}
			#playeranswer{
				width: 60%;
                height: 20vh;
                font-size: 100px;
                text-align: center;
                margin: 0px; 
                border: solid;
                border-width: 1px;
                border-radius: 20px;
				color: black;
			}
			#imgstatus{
					height: 30vh;
					weight: 30vw;
					float: right;
			}
			.margins{
				margin: -30px;
			}
			.boton {
		    	background-color: #A4978E;
			    color: white;
			    padding: 10px 20px;
			    border: none;
			    cursor: pointer;
		    	width: 30%;
			    opacity: 0.8;
			    height: 45%;
                font-size: 30px;
                border-width: 1px;
                border-radius: 20px;
			}
            .bg-color{
                background: #b5bdc8;
                background: -moz-linear-gradient(top, #b5bdc8 0%, #828c95 19%, #28343b 100%);
                background: -webkit-linear-gradient(top, #b5bdc8 0%,#828c95 19%,#28343b 100%);
                background: linear-gradient(to bottom, #b5bdc8 0%,#828c95 19%,#28343b 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b5bdc8', endColorstr='#28343b',GradientType=0 );
            }
			@media only screen and (max-width: 600px){
				.logo{
					height: 5em;
					width: 5em;
					float: left;
					margin-left: 5px;
				}
				.logoname{
					height: 5em;
					width: 16em;
					float: center;
					margin-left: 5px;
					
				}
				#logout{
					float: right;
					margin: -100px 5px; 
				}
				.phone-info{
					font-size: 25px;
				}
				h1{
				font-size: 15px;
				color: white;
				}
				.h1{
					font-size: 60px;
					font-family: 'Gloria Hallelujah', cursive;
					text-shadow: 2px 4px #525151;
				}
				.orb {			
					display: inline-block;				
					transition: all 2s ease-out;
				}
				.orb:hover {
					transform: scale(1.5);
					opacity: 1;
					cursor: pointer;
					animation-play-state: paused;
				}
				#statetext{
					font-size: 30px;
					margin-top: 50px;
				   
				}
				#question{				
					font-family: 'Nanum Pen Script', cursive;
					font-size: 35px;
				}
				#playeranswer{
					width: 60%;
					height: 20vh;
					font-size: 100px;
					text-align: center;
					margin: 0px; 
					border: solid;
					border-width: 1px;
					border-radius: 20px;
					color: black;
				}
				#imgstatus{
						height: 30vh;
						weight: 30vw;
						float: right;
				}
				.margins{
					margin: -30px;
				}
				.boton {
					background-color: #A4978E;
					color: white;
					padding: 10px 20px;
					border: none;
					cursor: pointer;
					width: 30%;
					opacity: 0.8;
					height: 45%;
					font-size: 10px;
					border-width: 1px;
					border-radius: 10px;
				}
                
			}
			
		</style>
		<script type="text/javascript" src="js/patal3.js"></script>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
		
    </head>
    <body onload = "generateQuestion()" class="bg-color">
		<div class="site-bg"></div>
        <div class="site-bg-overlay"></div>
		<!-- TOP HEADER -->
        <div class="top-header" style="height: 7em">
			<img src="images/logo.png" alt="" class="logo"  >
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <br><p class="phone-info" >Welcome <a><?php echo htmlspecialchars($_SESSION["username"]); ?></a>!</p>
						
                    </div>      
						<img src="images/patalino.png" alt="" class="logoname">
						<a class="btn btn-primary" id="logout" href="logout.php">Logout</a>
 
                </div>                
            </div>            
        </div> <!-- .top-header -->

        <div class="visible-xs visible-sm responsive-menu">
            <a href="#" class="toggle-menu">
                <i class="fa fa-bars"></i> Show Menu
            </a>
            <div class="show-menu">
                <ul class="main-menu">
						<li>
							<a href="index.php" ><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="rankings.php"><i class="fa fa-bar-chart"></i>Ranks</a>
                        </li>
                        
                </ul>
            </div>
        </div>

        <div class="container" id="page-content">
           <div class="row" >                
                <div class="col-md-12 col-sm-12 content-holder">
                    <!-- CONTENT -->
                    <div id="menu-container">                       
                       <div class="col-md-3 hidden-sm">
                    
							<nav id="nav" class="main-navigation hidden-xs hidden-sm">
  				                               
								<a  href="index.php"><i class="fa fa-home margins"></i>Home</a>
								<a  href="rankings.php"><i class="fa fa-bar-chart margins"></i>Ranks</a>
                            </nav>
						</div>
                        <div class="row rounded" id="game">				
                            
							<h1 id ="globaltimer"></h1>
							<h1 id ="scoreText">Score: 0</h1>
							<h1 id ="timer"></h1>
							<img src = "images/logo.png" id="imgstatus" >  
							<h1 id ="question"></h1> <br />
							
						
							<input type="text" id="playeranswer" onkeydown = "if (event.keyCode == 13) document.getElementById('submit').click()">
							<form method="POST" id="details" action="add_data.php">
								<input type="hidden" name="scoretotal" id="scorehidden" />
								<input type="hidden" name="corrAns" id="corrAnsHidden" />
								<input type="hidden" name="wrongAns" id="wrongAnsHidden" />
								<input type="hidden" name="numQues" id="numQuesHidden" />
							</form>


							<input type="button" class="boton" id="submit" value="Submit Answer" onclick="checkAnswer()">
							<input type="button" class="boton" id="reset" value="Reset Game" onclick="resetGame()" disabled = true>
							
								<br />
								
						</div>
							<b><p id ="statetext" style="text-align: center"></p></b>
                    </div>
				</div>
			</div>
		</div>
                
            <!-- SITE-FOOTER -->
        <div class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>
                        	<strong>Copyright &copy; 2019 <a href="#">PARS</a>.</strong>  All rights reserved.
                        
						</p>
                    </div>
                </div>
            </div>
        </div> <!-- .site-footer -->

        <script src="js/vendor/jquery-1.10.2.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>